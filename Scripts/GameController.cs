﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    /// <summary>
    /// Oyun Kontrol Enum
    /// </summary>
    [HideInInspector] public enum GameControl { EMPTY,START_GAME, CHECKPOINT,FINISHGAME,STAY }

    /// <summary>
    /// Oyun Kontrol Enum Degiskeni
    /// </summary>
    [HideInInspector] public GameControl gameControl;

    /// <summary>
    /// CheckPoint noktasinin hangisi ise pozisyonunu kayit edecegimiz degisken
    /// </summary>
    [HideInInspector] public Vector3 checkPointPos=Vector3.zero;

    /// <summary>
    /// Finish aninda kapatilacak objlerler
    /// </summary>
    [SerializeField] private GameObject _lineRenderer, drawImage;
    
    /// <summary>
    /// Karakter Transform
    /// </summary>
    [SerializeField] private Transform _myCar;
   
    /// <summary>
    /// Kameramin Transformu
    /// </summary>
    private Transform _cameraTransform;

    /// <summary>
    /// Kamera ile araba arasindaki mesafe
    /// </summary>
    private Vector3 _cameraMyCarDist;
    
    private void Awake()
    {
        gameControl = GameControl.START_GAME;
        _cameraTransform = Camera.main.transform;
        _cameraMyCarDist = _cameraTransform.localPosition - _myCar.position;
    }
    
    private void FixedUpdate()
    {
        if (gameControl != GameControl.FINISHGAME)
        {
            _cameraTransform.position = _cameraMyCarDist + _myCar.position;
        }
        else
        {
            _cameraTransform.position = _cameraMyCarDist + _myCar.position;
            _lineRenderer.SetActive(false);
            drawImage.SetActive(false);
            _cameraTransform.GetComponent<Animator>().enabled = true;

        }
        
    }
}
