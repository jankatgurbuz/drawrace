﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCarControl : MonoBehaviour
{
    /// <summary>
    /// Tekerlek colliderlari
    /// </summary>
    [SerializeField] private WheelCollider frontRight, frontLeft, backRight, backLeft;
    private GameController _gameController;
    private DrawingController _drawingController;

    private void Start()
    {
        Init();
    }
    private void Init()
    {
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        _drawingController = GameObject.Find("DrawingController").GetComponent<DrawingController>();
    }
    
    private void FixedUpdate()
    {
        // Oyun "STAY" durumundayken araba hareket eder.
        switch (_gameController.gameControl)
        {
            case GameController.GameControl.START_GAME:
                frontRight.motorTorque = 0;
                frontLeft.motorTorque = 0;
                backRight.motorTorque = 0;
                backLeft.motorTorque = 0;
                break;
            case GameController.GameControl.STAY:
                
                frontRight.motorTorque = 1000;
                frontLeft.motorTorque = 1000;
                backRight.motorTorque = 1000;
                backLeft.motorTorque = 1000;
                break;
        }
    }

    /// <summary>
    /// Temas Kontrolleri
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "CheckPointTag")
        {
            _gameController.checkPointPos = other.transform.position;
        }
        if (other.tag == "FallingTag")
        {
            _gameController.gameControl = GameController.GameControl.START_GAME;
            _drawingController.CheckPoint();
        }
        if (other.tag == "Finish")
        {
            _gameController.gameControl = GameController.GameControl.FINISHGAME;
            other.transform.GetChild(0).gameObject.SetActive(true);
            
        }
    }
}
