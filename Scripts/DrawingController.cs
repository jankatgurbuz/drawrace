﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using UnityEngine.UI;

public class DrawingController : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// Ne kadar cizim noktasi olacak bunu belirliyoruz 
    /// </summary>
    [Tooltip(" Cizimde kac point olacak ? ")]
    [SerializeField] [Range(100, 1000)] private int _positionCapacity = 100;

    /// <summary>
    /// Arabamizin son hali parent olarak aliyoruz. Olusan objeleri (tekerler,cizim(3b) vb.) child olarak ayarlanacak.
    /// </summary>
    [SerializeField] private GameObject _myCar;

    /// <summary>
    /// cizilen sekli 3b  ye cevirir. 
    /// </summary>
    [SerializeField] private TubeRenderer _tubeRenderer;

    /// <summary>
    /// Ekranin basildigi noktadan plane e dogru cizilen ray
    /// </summary>
    private Ray _ray;

    /// <summary>
    /// UI kutusunun icerisine cizim yapilmasi icin kullanilan birlesen.
    /// </summary>
    private LineRenderer _lineRenderer;

    /// <summary>
    /// lineRendererda pozisyonlari gezmek kullanilan sayici
    /// </summary>
    private int _lNPositionCounter = 0;

    /// <summary>
    /// Pozisyon kapasitesini bittip bitmedigini kontrol eder.
    /// </summary>
    private bool _positioncCapsityControl = true;

    /// <summary>
    /// On ve Arka tekerlek parentlarini alir.
    /// </summary>
    private Transform frontWheels, backWheels;

    /// <summary>
    /// Karakterimizin rigidbodysi
    /// </summary>
    private Rigidbody _myCarRigidBody;

    private GameController _gameController;

    /// <summary>
    /// hizimiz sifirlanmamasi icin tutulan temp degiskeni
    /// </summary>
    private Vector3 _tempVelocity;

    #endregion

    private void Start()
    {
        Init();
        StartLeanTouch();
        StartLineRenderer();
    }

    /// <summary>
    /// Atamalar gerceklesir.
    /// </summary>
    private void Init()
    {
        frontWheels = GameObject.Find("FrontWheels").transform;
        backWheels = GameObject.Find("BackWheels").transform;
        frontWheels.gameObject.SetActive(false);
        backWheels.gameObject.SetActive(false);

        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        _myCarRigidBody = _myCar.GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Start aninda LeanTouch Eventlari ayarlanir.
    /// </summary>
    private void StartLeanTouch()
    {
        LeanTouch.OnFingerSet += OnFingerSet;
        LeanTouch.OnFingerUp += OnFingerUp;
        LeanTouch.OnFingerDown += OnFingerDown;
    }

    /// <summary>
    /// Line Renderer olusturulur.
    /// </summary>
    private void StartLineRenderer()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        _lineRenderer.widthMultiplier = 0.2f;
    }

    /// <summary>
    /// Ekrana ilk dokunma ani
    /// </summary>
    /// <param name="obj"></param>
    private void OnFingerDown(LeanFinger obj)
    {
        //hizimizi bir temp degiskene atiyoruz
        _tempVelocity = _myCarRigidBody.velocity;
        
    }


    /// <summary>
    /// ekrana basilma degisikligini algilar
    /// </summary>
    /// <param name="obj"></param>
    private void OnFingerSet(LeanFinger obj)
    {
        if (obj.IsOverGui && _positioncCapsityControl)
        {
            //Sahneye bir ray olusturuluyor
            _ray = Camera.main.ScreenPointToRay(obj.ScreenPosition);

            // bir Plane olusturuluyor rayin origininin 10 birim ilerisinde (Sanal bir plane)
            Plane xy = new Plane(Vector3.forward, _ray.origin + new Vector3(0, 0, 10));

            // Buraya rayCast cizdiriyoruz
            xy.Raycast(_ray, out float distance);

            //Debug.DrawRay(_ray.origin, _ray.direction, Color.red, 10);

            //rayin temas noktasi
            _ray.GetPoint(distance);

            //kapasite kadar lineRenderer icerisindeki pozisyonlar doluyor
            if (_lNPositionCounter <= _positionCapacity)
            {
                _lineRenderer.positionCount = _lNPositionCounter + 1;
                _lineRenderer.SetPosition(_lNPositionCounter++, transform.InverseTransformPoint(_ray.GetPoint(distance)));
            }
            else
            {
                //kapasite doldugunda false yapiyor.
                _positioncCapsityControl = false;
            }
        }
    }

    /// <summary>
    /// Ekrandan elimizi cekersek tetiklenir.
    /// </summary>
    /// <param name="obj"></param>
    private void OnFingerUp(LeanFinger obj)
    {
        StartCoroutine(OnFingerUpCoroutine());
    }
    private IEnumerator OnFingerUpCoroutine()
    {
        MinAndMaxPoints(out float min, out float max, out float totalDistance);
       
        if (totalDistance > 1 && totalDistance < 20)
        {
            _myCar.transform.rotation = Quaternion.Euler(0, 0, 0);
            // ilk ekrana dokunuldugunda calisir ve ekrandaki objeleri acar.
            if (_gameController.gameControl == GameController.GameControl.START_GAME)
            {
                _gameController.gameControl = GameController.GameControl.STAY;
                FirstTouchOnTheScreen();
            }

            // TubeRender Scriptini hazirliyoruz var ise icinde collider onlari bosaltiyoruz.
            PrepairingTubeRenderer();
            
            // orta noktasi bulunuyor UI den olusan objenin
            float x = (min + max) / 2;
            _tubeRenderer.transform.localPosition = new Vector3(x, _lineRenderer.GetPosition(_lineRenderer.positionCount / 2).y,
                _lineRenderer.GetPosition(_lineRenderer.positionCount / 2).z);

            // Tube rendererda kac tane pozisyon olacak birlestirilip bir obje haline getirilecek onu yapiyoruz WaitForFixedUpdate e kadar.

            _tubeRenderer._positions = new Vector3[_lineRenderer.positionCount];

            for (int i = 0; i < _tubeRenderer._positions.Length; i++)
            {
                _tubeRenderer._positions[i] = _lineRenderer.GetPosition(i);
                SphereCollider sphrCol = _tubeRenderer.gameObject.AddComponent<SphereCollider>();
                sphrCol.radius = 0.1f;
                sphrCol.center = _tubeRenderer.gameObject.transform.InverseTransformPoint(_lineRenderer.GetPosition(i));
            }

            //Mesh olustur.
            _tubeRenderer.Run();
            _tubeRenderer.GetComponent<MeshRenderer>().enabled = false;
            
            // Tekerlekler ilk ve son konuma yerlestiriliyor
            frontWheels.localPosition = _lineRenderer.GetPosition(0);
            backWheels.localPosition = _lineRenderer.GetPosition(_lineRenderer.positionCount - 1);

            //  TubeRenderer icerisine gonderiliyor
            frontWheels.SetParent(_tubeRenderer.transform);
            backWheels.SetParent(_tubeRenderer.transform);

            // MyCar Objesinin icerisine gonderiyoruz TubeRendereri (Icerisinde tekerlekler de var)
            // pozisyonu sifirliyoruz ve rotationunu duzenliyoruz
            _tubeRenderer.transform.SetParent(_myCar.transform);
            _tubeRenderer.transform.localPosition = Vector3.zero;
            _tubeRenderer.transform.rotation = Quaternion.Euler(0, -90, -10);

            //Tekerlekler alt nesne olduklari icin parentin rotationunu aldilar. Tekrardan sifirliyoruz
            frontWheels.rotation = Quaternion.Euler(0, 0, 0);
            backWheels.rotation = Quaternion.Euler(0, 0, 0);

            //bir sonraki cizim icin _TubeRenderer icerisindeki position dizisini cop toplayicilara yolluyoruz :) 
            _tubeRenderer._positions = null;

            //UI uzerinde cizim yapacagimiz LineRenderer positionCount u sifirliyoruz
            // Boylece bir onceki cizim gitmis oluyor.
            _lineRenderer.positionCount = 0;
            _lNPositionCounter = 0;

            _myCar.transform.position = new Vector3(_myCar.transform.position.x, _myCar.transform.position.y + 5, _myCar.transform.position.z);

            // Velocitimizi gerisin geri atiyoruz.
            _myCarRigidBody.velocity = _tempVelocity;
            _myCarRigidBody.isKinematic = false;
            _positioncCapsityControl = true;
            yield return new WaitForEndOfFrame();
            _tubeRenderer.GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            _lineRenderer.positionCount = 0;
            _lNPositionCounter = 0;
            _positioncCapsityControl = true;
        }
    }

    /// <summary>
    /// Ilk oyuna baslanildiginda Objeleri acmak icin
    /// </summary>
    private void FirstTouchOnTheScreen()
    {
        _tubeRenderer.gameObject.SetActive(true);
        frontWheels.gameObject.SetActive(true);
        backWheels.gameObject.SetActive(true);
        _myCarRigidBody.isKinematic = false;

    }

    /// <summary>
    /// TubeRenderer objesinde collider var ise silmek icin cagiriyoruz ve gerekli duzenlemelerini yapiyoruz.
    /// </summary>
    private void PrepairingTubeRenderer()
    {
        _myCarRigidBody.isKinematic = true;
        
        // En disa cikariyorum pozisyonunu set ettikten sonra tekrardan iceri alacagim
        // NOT ! Bu kisim duzeltilebilir daha optimize bir yontem dusunulebilir
        _tubeRenderer.transform.SetParent(transform.parent.parent);

        // Rotation sifirlaniyor
        _tubeRenderer.transform.rotation = Quaternion.Euler(0, 0, 0);

        frontWheels.SetParent(transform.parent.parent);
        backWheels.SetParent(transform.parent.parent);

        // Sphere Colliderlar siliniyor
        foreach (var item in _tubeRenderer.GetComponents<SphereCollider>())
        {
            Destroy(item);
        }
    }

    /// <summary>
    /// Line renderer da min max noktasini buluyor
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    private void MinAndMaxPoints(out float min, out float max, out float totalDistance)
    {
        if (_lineRenderer.positionCount > 0)
        {
            float minPoint = _lineRenderer.GetPosition(0).x;
            float maxPoint = _lineRenderer.GetPosition(0).x;
            float totalDist = 0;
            for (int i = 0; i < _lineRenderer.positionCount; i++)
            {
                if (i > 0)
                {
                    totalDist += Vector3.Distance(_lineRenderer.GetPosition(i), _lineRenderer.GetPosition(i - 1));
                }
                if (_lineRenderer.GetPosition(i).x < minPoint)
                {
                    minPoint = _lineRenderer.GetPosition(i).x;
                }
                if (_lineRenderer.GetPosition(i).x > maxPoint)
                {
                    maxPoint = _lineRenderer.GetPosition(i).x;
                }
            }
            min = minPoint;
            max = maxPoint;
            totalDistance = totalDist;
        }
        else
        {
            min = 0;
            max = 0;
            totalDistance = 0;
        }


    }

    /// <summary>
    /// Karakter dustugunde bir yerden CheckPoint noktasindan baslamasi icin bu metodu cagiriyoruz.
    /// </summary>
    public void CheckPoint()
    {
        frontWheels.gameObject.SetActive(false);
        backWheels.gameObject.SetActive(false);

        _myCarRigidBody.isKinematic = true;
        _myCarRigidBody.velocity = Vector3.zero;
        _myCar.transform.position = _gameController.checkPointPos;
        _tubeRenderer.gameObject.SetActive(false);

    }
}
